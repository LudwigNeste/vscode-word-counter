// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Count Words');

	let selection: string = "";
	let statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100.4);
	statusBarItem.show();

	vscode.window.onDidChangeVisibleTextEditors(e => {
		if(e.length = 0){
			statusBarItem.hide();
		}else{
			statusBarItem.text = countEditor(e[0]);
		}
	});
	vscode.window.onDidChangeTextEditorSelection(e => {
		statusBarItem.text = countEditor(e.textEditor);
	});

	if(vscode.window.activeTextEditor){
		statusBarItem.text = countEditor(vscode.window.activeTextEditor);
	}
	context.subscriptions.push(statusBarItem);
}

// this method is called when your extension is deactivated
export function deactivate() {}

function count(s: string): number {
	let wordCount = 0;
	const seperators = {" ": null, "\n": null};
	if (s != ""){
		wordCount = 0;
		let foundWord = false;
		for (let char of s) {
			if(char in seperators){
				foundWord = false;
			}else{
				if (foundWord == false){
					wordCount += 1;
					foundWord = true;
				}
			}
		}
	}
	return wordCount;
}

function uintToNormString(num: number): string {
	let text = "";
	if(num < 10) {
		text = "00"+num;
	}else if(num < 100) {
		text = "0"+num;
	}else{
		text = num.toString();
	}
	return text;
}

function countEditor(textEditor: vscode.TextEditor): string{
	let selection = "";
	for (let sel of textEditor.selections) {
		selection += textEditor.document.getText(sel);
	}
	selection = selection.trim();

	let wordCountSel = count(selection);
	let wordCount = count(textEditor.document.getText());

	let wordText = "Words: "+wordCount.toString();
	if (wordCountSel > 0) {
		wordText += " (selected: "+uintToNormString(wordCountSel)+")";
	}
	return wordText;
}