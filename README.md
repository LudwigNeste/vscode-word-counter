# counter README

A very small extension that counts the words in the document and the selected ones. I defined the word count to be equivalent to
~~~javascript
string.split(" ").split("\n").length
~~~
I know, that this is not the perfect way of doing it, but it just fits my needs for now.

## Features

Shows a little word counter in the status bar, which counts the word and the selected ones. 

![Demo Big](images/demo2.png)

![Demo Small](images/demo1.png)


## Requirements

None

## Extension Settings

None

## Known Issues

- slow/ugly implementation of counting.
- imperfect definition of "word"

## Release Notes

Isn't the code documentation enough?

### 1.0.0

Inital release.

